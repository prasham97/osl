# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 11:55:08 2018

@author: sheth
"""

import re
import csv
import datetime
import urllib.request as u
try:
    url = "https://www.99acres.com/property-in-ahmedabad-ffid?keyword="
    area = input ("Enter the Area:")
    url+=area
    hdr = {'User-Agent':'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    req=u.Request(url, headers=hdr)
    response = u.urlopen(req)
    data = response.read()
    
    data1=data.decode("utf-8")
    m=re.search('meta name = "description" content',data1)
    
    start=m.start()
    end= start+200
    string=data1[start:end]
    m=re.search('"Find',string)
    start=m.start()
    start = start+6
    string=string[start:]
    m=re.search('Properties',string)
    end=m.start()
    string=string[0:(end-2)]
    print(string + " properties found for sale")
except:
    string = "NA"
finally:   
    time =datetime.datetime.now()
    mydata= [[area] , [string] , [time]]
    myfile= open('temp.csv','a')
    with myfile:
        writer=csv.writer(myfile)
        writer.writerows(mydata)
    
    input("press any key to exit")
